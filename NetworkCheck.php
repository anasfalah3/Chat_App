<div class="notification-Field">
      <div class="toast">
            <div class="content">
                  <div class="icon"><i class="uil uil-wifi"></i></div>
                  <div class="details">
                        <span></span>
                        <p>Hurray! Internet is connected.</p>
                  </div>
            </div>
            <div class="close-icon"><i class="uil uil-times"></i></div>
      </div>
</div>
<script src="javascript/NetworkCheck.js"></script>